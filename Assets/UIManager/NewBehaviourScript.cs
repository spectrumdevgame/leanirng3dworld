﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject player;
    private QuestTracker playerController;
    public Canvas activeQuests;

    private int initx   = 60;
    private int inity = 0;
    private int addedMessages = 0;

    Dictionary<QuestBehavior, GameObject> messages;

    void Start()
    {
        this.messages = new Dictionary<QuestBehavior, GameObject>();
        playerController = player.GetComponent("QuestTracker") as QuestTracker;
    }

    // Update is called once per frame
    void Update()
    {
        List<QuestBehavior> quests = playerController.getQuests();

        foreach(QuestBehavior quest in quests)
        {
            this.addMessageToCanvas(quest);
        }
    }

    public void addMessageToCanvas(QuestBehavior quest)
    {

        if (this.messages.ContainsKey(quest) ==false)
        {

            GameObject newGO = this.createTextComponent(quest.giveStatus(), this.initx , this.inity + this.addedMessages);

            this.addedMessages += 25;

            newGO.transform.SetParent(this.activeQuests.transform);

            this.messages.Add(quest, newGO);
        }
        else
        {
            GameObject textOBj = this.messages[quest];

            textOBj.GetComponent<Text>().text = quest.giveStatus();
        }

    }

    private GameObject createTextComponent(string message,int x,int y)
    {
        GameObject newGO = new GameObject("text");

        RectTransform trans = newGO.AddComponent<RectTransform>();

        trans.localPosition = new Vector3(x, y, 0);
   
        Text text = newGO.AddComponent<Text>();

        text.text = message;

        text.fontSize = 10;

        text.color = Color.black;

        Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");

        text.font = ArialFont;
       
        return newGO;
    }



}
