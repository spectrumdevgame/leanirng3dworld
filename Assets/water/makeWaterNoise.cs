﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class makeWaterNoise : MonoBehaviour
{

    public float power = 3;
    public float scale = 1;
    public float timeScale = 1;


    private float xOffset;
    private float yOffset;

    private MeshFilter mf;

    public int gridSize = 16;



    // Start is called before the first frame update
    void Start()
    {
        mf = GetComponent<MeshFilter>();
        mf.mesh = generateMesh();
        MakeNosie();
    }

    // Update is called once per frame
    void Update()
    {
        MakeNosie();
        xOffset += Time.deltaTime + timeScale;
        yOffset += Time.deltaTime + timeScale;
    }


    private void MakeNosie()
    {
        Vector3[] vertices = mf.mesh.vertices;

        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i].y = CalculateHeight(vertices[i].x, vertices[i].z) * power;
        }

        mf.mesh.vertices = vertices;
    }

    private float CalculateHeight(float x, float y)
    {
        float xcord = x + scale + xOffset;
        float ycord = y + scale + yOffset;
        return Mathf.PerlinNoise(xcord, ycord);
    }


    private Mesh generateMesh()
    {
        Mesh mesh = new Mesh();

        List<Vector3> vertices = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();

        for (int x = 0; x <= gridSize; x++)
        {
            for (int y = 0; y <= gridSize; y++)
            {
                vertices.Add(new Vector3(-gridSize * 0.5f + gridSize * (x / (float)gridSize), 0, -gridSize * 0.5f + gridSize * (y / (float)gridSize)));
                normals.Add(Vector3.up);
                uvs.Add(new Vector2(x / (float)gridSize, y / (float)gridSize));
            }
        }

        List<int> triangles = new List<int>();

        int verCount = gridSize + 1;

        for (int i = 0; i < verCount * verCount - verCount; i++)
        {
            if ((i + 1) % verCount == 0)
            {
                continue;
            }

            triangles.AddRange(new List<int>()
            {
                i+1+verCount,i+verCount,i,i,i+1,i+verCount+1
            }
            );

        }

        mesh.SetVertices(vertices);
        mesh.SetNormals(normals);
        mesh.SetUVs(0, uvs);
        mesh.SetTriangles(triangles, 0);



        return mesh;
    }
}
