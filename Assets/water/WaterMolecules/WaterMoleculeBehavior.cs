﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class WaterMoleculeBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    public Rigidbody rb;
   
    public Vector3 target;
    public float thrust = 1.0f;
    void Start()
    {
        target = new Vector3(-56.79f, -32.8f, -67.75f);
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.AddForce(target * 0.02f);
        Debug.Log("Adding Force");
    }

}
