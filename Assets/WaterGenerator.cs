﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGenerator : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject WaterMolecule;

    public int numberOfMolecules=10;

    private int instantiatedWaterMolecules;

    void Start()
    {
        instantiatedWaterMolecules = 0;

        InvokeRepeating("instantiateWaterMolecule", 2.0f, 0.03f);

    }

    private void instantiateWaterMolecule()
    {
        if (instantiatedWaterMolecules < numberOfMolecules)
        {
            Instantiate(WaterMolecule, this.transform.position, transform.rotation);
            instantiatedWaterMolecules++;
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
