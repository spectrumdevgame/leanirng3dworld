﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class QuestBehavior
{
    private bool completed;
    public abstract bool NewItemFound(Collider other);

    public abstract bool isDestroyItem();

    public abstract bool destroyQuestGiver();

    public abstract string giveStatus();


    private bool destroyItem;

    public bool isCompleted()
    {
        return this.completed;
    }

    public void setisCompleted(bool status)
    {
        this.completed = status;
    }

    public abstract void tellUser();
}
