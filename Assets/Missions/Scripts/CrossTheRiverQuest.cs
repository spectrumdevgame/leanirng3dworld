﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class CrossTheRiverQuest : QuestBehavior
{

    private int numberOfCrossings = 1;

    private int currentNumberOfCrossings = 0;

    public override bool destroyQuestGiver()
    {
        return false;
    }

    public override string giveStatus()
    {
        if (this.isCompleted())
        {
            return "You already crossed the bridge!";
        }
        else
        {
            return "You havent crossed the bridge!";
        }
        
    }

    public override bool isDestroyItem()
    {
        return false;
    }

    public override bool NewItemFound(Collider other)
    {
        if (other.CompareTag("quest_detector=river"))
        {
            this.currentNumberOfCrossings++;
            if (currentNumberOfCrossings == numberOfCrossings)
            {
                this.setisCompleted(true);
            }

            return true;
        }
        else
        {
            return false;
        }
        
    }

    public override void tellUser()
    {
        Debug.Log(this.giveStatus());
    }
}
