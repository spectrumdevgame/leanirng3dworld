﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldQuests
{
   
    public static QuestBehavior createQuestBehavior(worldQuestsEnum create)
    {
        switch (create)
        {
            case worldQuestsEnum.CrossTheRiver:
                return new CrossTheRiverQuest();

            case worldQuestsEnum.CollectCoins:
                return new CollectRingsQuest();

            case worldQuestsEnum.huntBirds:
                return new HuntBirds();
        }

        return null;
    }
}


public enum worldQuestsEnum
{
    CrossTheRiver,CollectCoins,huntBirds
}
