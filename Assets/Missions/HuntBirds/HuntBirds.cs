﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuntBirds : QuestBehavior
{
    // Start is called before the first frame update

    private static int huntedBirds = 15;
    private int cuurenthuntedBirds = 0;

    public override bool destroyQuestGiver()
    {
        return true;
    }
    public override string giveStatus()
    {
        return "You have hunted " + this.cuurenthuntedBirds + " Birds!";
    }

    public override bool isDestroyItem()
    {
        return true;
    }

    public override bool NewItemFound(Collider other)
    {
        if (other.CompareTag("quest_detector=bird"))
        {
            this.cuurenthuntedBirds++;

            if (huntedBirds == cuurenthuntedBirds)
            {
                this.setisCompleted(true);
            }

            return true;
        }
        else
        {
            return false;
        }

    }

    public override void tellUser()
    {
        Debug.Log(this.giveStatus());
    }
}
