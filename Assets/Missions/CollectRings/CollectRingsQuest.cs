﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CollectRingsQuest : QuestBehavior
{
    // Start is called before the first frame update

    private int collectableRings = 10;
    private int cuurentCollectedleRings = 0;

    public override bool destroyQuestGiver()
    {
        return true;
    }
    public override string giveStatus()
    {
       return "You have collected "+this.cuurentCollectedleRings+" rings!";
    }

    public override bool isDestroyItem()
    {
        return true;
    }

    public override bool NewItemFound(Collider other)
    {
        if (other.CompareTag("quest_detector=ring"))
        {
            this.cuurentCollectedleRings++;

            if (collectableRings == cuurentCollectedleRings)
            {
                this.setisCompleted(true);
            }

            return true;
        }
        else
        {
            return false;
        }

    }

    public override void tellUser()
    {
        Debug.Log(this.giveStatus());
    }
}
