﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestTracker : MonoBehaviour
{
    // Start is called before the first frame update


    private List<QuestBehavior> quests;


    private List<worldQuestsEnum> activeQuests;

    void Start()
    {
        quests = new List<QuestBehavior>();
        this.activeQuests = new List<worldQuestsEnum>();
    }


    public List<QuestBehavior> getQuests()
    {
        return this.quests;
    }


    

    void OnTriggerEnter(Collider other)
    {

        if (other.transform.CompareTag("quest"))
        {
            QuestManager aux = other.gameObject.GetComponent("QuestManager") as QuestManager;
            QuestBehavior newQuest = WorldQuests.createQuestBehavior(aux.QuestBehaviorEnum);

            if (this.activeQuests.Contains(aux.QuestBehaviorEnum))
            {
                if (newQuest.destroyQuestGiver() == false)
                {
                    this.quests.Add(newQuest);
                    this.activeQuests.Add(aux.QuestBehaviorEnum);
                }
            }
            else
            {
                this.quests.Add(newQuest);
                this.activeQuests.Add(aux.QuestBehaviorEnum);
            }

            
        }
        else if (other.transform.tag.Contains("quest_detector"))
        {
            foreach (QuestBehavior quest in this.quests)
            {
                if (quest.isCompleted() == false)
                {
                    if (quest.NewItemFound(other))
                    {
                        if (quest.isDestroyItem())
                        {
                            Destroy(other.gameObject);
                        }
                    }

                    if (quest.isCompleted())
                    {
                        quest.tellUser();
                        if (quest.destroyQuestGiver())
                        {
                            Destroy(other.gameObject);
                        }
                    }
                }
            }
        }

    }
}
