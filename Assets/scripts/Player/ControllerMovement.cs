﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControllerMovement : MonoBehaviour
{


    public float heroRotationSpeed;
    public float heroSpeed;

    public CharacterController hero;

    public float heroGavity = 9.82f;

    public float jumpForce;

    private Vector3 camForward;

    private Vector3 camRight;

    private Vector3 heroInput;

    private Vector3 movePlayer;

    private float horizontalMove;

    private float verticalMove;

    private float fallVelocity;

    public bool isOnSlope;

    public bool isOnSmallSlope;

    public float slideVelocity = 5f;
    public float slideForce = 5f;

    private Vector3 floorNormal;

    public bool hasJumped;




    // Start is called before the first frame update
    void Start()
    {
        this.hero = GetComponent<CharacterController>();
        this.hasJumped = false;
        this.hero.detectCollisions = true;
       
    }



    // Update is called once per frame
    void Update()
    {
        this.horizontalMove = Input.GetAxis("Horizontal");

        this.verticalMove = Input.GetAxis("Vertical");

        bool jumping = Input.GetButtonDown("Jump");

        this.heroInput = new Vector3(0, 0, this.verticalMove);
        
        this.heroInput = Vector3.ClampMagnitude(this.heroInput,1f);

        lookingAt();
        
        this.movePlayer = this.heroInput.x * this.camRight + this.heroInput.z* this.camForward;
        
        this.movePlayer *= heroSpeed;

        transform.Rotate(0, this.horizontalMove * heroRotationSpeed * Time.deltaTime, 0);

        this.setGravity();
        this.jump(jumping);

        this.hero.Move(this.movePlayer *  Time.deltaTime);

    }

    private void jump(bool jumping)
    {
        

        if ( this.hasJumped==false && jumping)
        {
            this.fallVelocity = this.jumpForce;
            movePlayer.y = this.fallVelocity;
            this.hasJumped = true;
        }
    }

    void setGravity()
    {
        if (this.hero.isGrounded || isOnSlope)
        {
            this.fallVelocity = -this.heroGavity * Time.deltaTime;
        }
        else
        {
            this.fallVelocity -= this.heroGavity*Time.deltaTime;
        }

        movePlayer.y = this.fallVelocity;

        //this.slideDonw();
        
    }

    void lookingAt()
    {
        this.camForward = transform.forward;
        this.camRight = transform.right;

        camForward.y = 0f;
        camRight.y = 0f;

        this.camForward = this.camForward.normalized;
    
        this.camRight = this.camRight.normalized;
    
    }

    public void slideDonw()
    {
        float currentAngle = Vector3.Angle(this.floorNormal, Vector3.up);

        isOnSmallSlope = currentAngle > 0.0f && currentAngle < this.hero.slopeLimit;

        isOnSlope = currentAngle > this.hero.slopeLimit;

        if (isOnSlope)
        {
           this.movePlayer.x += this.floorNormal.x + this.slideVelocity;
           this.movePlayer.z += this.floorNormal.z + this.slideVelocity;
            this.movePlayer.y += this.slideForce;
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        this.floorNormal = hit.normal;

        if (hit.transform.tag == "Floor")
        {
            this.hasJumped = false;
        }

    }


}
